# Projeto PFA

Esse projeto é um desafio 01 com foco no docker do programa PFA FullCycle.

O objetivo foi criar uma API Rest que faz uma listagem simples de um banco de dados e redirecionar as requisições da API usando o NGINX.

Foram criadas 3 imagens e uma network, segue os detalhes dela.

| objetos docker | Descrição |
| -------------- | --------- |
|rodolfo374/desafio1-pfa-db:1.0   | Imagem do banco de dados com uma tabela módulos com os dados polulados |
|rodolfo374/desafio1-pfa-app:1.0  | Imagem da API REST que lista os módulos |
|rodolfo374/desafio1-pfa-nginx:1.0| Imagem do NGINX para fazer o redirecionamento das requisições da API|
|desafio1-pfa                     | Network usada para fazer a comunicação com os containers executados a partir das imagens |

# O desafio foi usado as seguintes tecnologias

  - **Swagger** (Usado para documentar API e os end-points) 
  - **Docker**
  - **Java 11**
  - **Spring Boot 2.4.5 - Maven**
  - **Lombok** (Lombok é uma biblioteca java com foco em produtividade e redução de código)
  - **MySql** (Banco de dados)
  - **NGINX** (Servidor de proxy reverso)

 
# Executar o desafio

As imagens estão publicadas no Docker Hub, caso não queira criar as imagens local apenas execute os comandos de execução do container.

**Criar uma network**

A network foi criada para que os container conseguissem se comunicar entre si.
```sh
docker network create desafio1-pfa
```

**Criar uma imagem do banco de dados**

Gera uma imagem de banco criando a tabela e inserindo os dados.
```sh
docker build -t rodolfo374/desafio1-pfa-db:1.0 docker-mysql/.
```
Executa o container do banco de dados
```sh
docker run --network=desafio1-pfa --name=desafio1-pfa-db -d -e MYSQL_ROOT_PASSWORD=123Aa321 -e MYSQL_DATABASE=db-pfa rodolfo374/desafio1-pfa-db:1.0 
```

**Criar uma imagem da API**

Comando para gerar o pacote da API java

```sh
mvn clean install 
```
Gera uma imagem da API.

```sh
docker build -t rodolfo374/desafio1-pfa-app:1.0 .
```
Executa o container da API

```sh
docker run --network=desafio1-pfa --name=desafio1-pfa-app -d rodolfo374/desafio1-pfa-app:1.0
```
**Criar uma imagem da NGINX**

Gera uma imagem da NGINX.

```sh
docker build -t rodolfo374/desafio1-pfa-nginx:1.0 docker-nginx/.
```
Executa o container do NGINX

```sh
docker run --network=desafio1-pfa --name=desafio1-pfa-nginx -d -p 8080:80  rodolfo374/desafio1-pfa-nginx:1.0
```


# Teste

Teste a aplicação no Swagger ou fazendo a requisição do end-point no browser.

Swagger: http://localhost:8080/swagger-ui/index.html 

![header][image-swagger]


Browser: http://localhost:8080/modulo/findAll

![header][image-browser]





[image-swagger]: https://gitlab.com/rodolfo.rodrigues374/spring-pfa-desafio-1/-/raw/master/img/swagger.png
[image-browser]: https://gitlab.com/rodolfo.rodrigues374/spring-pfa-desafio-1/-/raw/master/img/browser.png

 