package pfa.com.entity;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "t_modulo")
public class ModuloEntity implements Serializable{
 
	/**
	 * 
	 */
	private static final long serialVersionUID = -4549850362242745947L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	@Column(name = "id_modulo")
	private BigInteger idModulo;
 
	@Column(name = "descricao", nullable = false, length = 100)
	private String descricao;
}
