package pfa.com.dto;

import java.math.BigInteger;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ModuloResponse {
	
	public ModuloResponse(BigInteger idProduto, String descricao) {
		this.idProduto = idProduto;
		this.descricao = descricao;
	}

	private BigInteger idProduto;
	private String descricao;
	
}
