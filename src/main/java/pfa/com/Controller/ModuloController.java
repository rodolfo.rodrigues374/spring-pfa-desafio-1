package pfa.com.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import pfa.com.entity.ModuloEntity;
import pfa.com.service.ModuloService;

@RestController
@RequestMapping("/modulo")
@Tag(name = "modulo", description = "Modulo API")
@SuppressWarnings({ "unchecked", "rawtypes" })
public class ModuloController {

	@Autowired
	private ModuloService moduloService;

	@Operation(summary = "Retorna uma lista de modulos", description = "Essa operação retorna as informações dos modulos.", tags = {
			"modulo" })
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Lista de modulos retornada com sucesso.", content = @Content(array = @ArraySchema(schema = @Schema(implementation = ModuloEntity.class)))) })
	@RequestMapping(value = "/findAll", method = RequestMethod.GET)
	public ResponseEntity pesquisaProduto() {

		return new ResponseEntity(this.moduloService.findAll(), HttpStatus.OK);

	}
}
