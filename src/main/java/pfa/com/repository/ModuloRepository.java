package pfa.com.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import pfa.com.dto.ModuloResponse;
import pfa.com.entity.ModuloEntity;

@Repository
public interface ModuloRepository extends JpaRepository<ModuloEntity, BigInteger>{

	
	@Query("SELECT new pfa.com.dto.ModuloResponse(m.idModulo, m.descricao) FROM pfa.com.entity.ModuloEntity m")
	List<ModuloResponse> findAllModulos();
}
