package pfa.com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pfa.com.dto.ModuloResponse;
import pfa.com.repository.ModuloRepository;

@Service
public class ModuloService {

	@Autowired
	private ModuloRepository moduloRepository;
	
	public List<ModuloResponse> findAll() {
		
		List<ModuloResponse> li = moduloRepository.findAllModulos(); 
		return li;
	}
}
