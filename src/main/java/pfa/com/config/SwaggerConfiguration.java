package pfa.com.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration 
public class SwaggerConfiguration {

	@Value("${application-name}")
	private String name;

	@Value("${application-version}")
	private String versao;

	@Value("${application-description}")
	private String descricao;
	
	@Value("${url-host}")
	private String host;
  
	
	private ApiInfo apiInfo() {
	
		  return new ApiInfoBuilder()
		            .title(name)
		            .description(descricao)
		            .version(versao)
		            .license("License Version 1.0")
		            .licenseUrl("https://gitlab.com/rodolfo.rodrigues374/spring-pfa-desafio-1")
		            .contact(new Contact("Rodolfo Rodrigues", "https://gitlab.com/rodolfo.rodrigues374/spring-pfa-desafio-1", "rodolfo.rodrigues374@gmail.com"))
		            .build(); 
	}

	@Bean
	public Docket api() {	
		return new Docket(DocumentationType.SWAGGER_2).host(host).select().paths(PathSelectors.any()).build().apiInfo(apiInfo());
	}

}
