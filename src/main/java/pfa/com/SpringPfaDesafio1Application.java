package pfa.com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication 
public class SpringPfaDesafio1Application {
 
	
	public static void main(String[] args) {
		SpringApplication.run(SpringPfaDesafio1Application.class, args);
			 
	}

}
