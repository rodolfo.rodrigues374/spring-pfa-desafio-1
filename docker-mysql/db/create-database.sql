CREATE TABLE t_modulo (
	id_modulo bigint(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	descricao VARCHAR(100) NOT NULL
);

SET character_set_client = utf8;
SET character_set_connection = utf8;
SET character_set_results = utf8;
SET collation_connection = utf8_general_ci;

insert into t_modulo (descricao) values ("Docker");
insert into t_modulo (descricao) values ("Fundamentos de Arquitetura de Software");
insert into t_modulo (descricao) values ("Comunicação");
insert into t_modulo (descricao) values ("RabbitMQ");
insert into t_modulo (descricao) values ("Autenticação e Keyclock");
insert into t_modulo (descricao) values ("Domain Drive Design e Arquitetura hexagonal");
insert into t_modulo (descricao) values ("Arquitetura do projeto prático - Codeflix");
insert into t_modulo (descricao) values ("Microsserviços: Cátalogo de vídeos com JAVA(Back-end)");

